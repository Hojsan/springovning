package com.example.springovning;


import lombok.Value;

@Value
public class UpdateAnimal {
    String name;
    String binomialName;
}
