package com.example.springovning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringOvningApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringOvningApplication.class, args);
    }

}
